public class App {
    public static void main(String[] args) throws Exception {
        Libro libro=new Libro("Rana", "autore", 4);
        Libro libro2=new Libro("titolo2", "autore", 4);
        Libro libro3=new Libro("titolo4", "autore2", 4);
        Libro libro4=new Libro("titolo3", "autore3", 4);
        Libro libro6=new Libro("titolo2", "autore4", 4);
        //System.out.println(libro.equals(libro, libro2));
        Mensola mensola=new Mensola();
        mensola.setVolume(libro, 0);
        mensola.setVolume(libro2, 1);
        mensola.setVolume(libro3, 2);
        mensola.setVolume(libro4, 3);
        mensola.setVolume(libro6, 5);
        //System.out.println(mensola.elencoLibriAutore("autore"));
        System.out.println(mensola);
        mensola.compatta();
        System.out.println(mensola);
        
    }
}
