public class Libro {
    /* ATTRIBUTI DI CLASSE */
    private String titolo, autore;
    private int numeroPagine;
    private static double costoPagina = 0.05;
    private final double COSTO_FISSO = 5.5;
    /* COSTRUTTORI */
    public Libro() {
        this.titolo = "indefinito";
        this.autore = "sconosciuto";
        this.numeroPagine = 0;
    }

    public Libro(String titolo, String autore, int numeroPagine) {
        this.titolo = titolo;
        this.autore = autore;
        this.numeroPagine = numeroPagine;
    }

    public Libro(Libro libro) {
        setTitolo(libro.getTitolo());
        setAutore(libro.getAutore());
        setNumeroPagine(libro.getNumeroPagine());
    }

    public String getTitolo() {
        return this.titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getAutore() {
        return this.autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public int getNumeroPagine() {
        return this.numeroPagine;
    }

    public void setNumeroPagine(int numeroPagine) {
        this.numeroPagine = numeroPagine;
    }
    
    public double prezzo() {
        double prezzo = COSTO_FISSO + costoPagina * numeroPagine;
        return prezzo;
    }

    public static void setCostoPagina(double costo) {
        costoPagina = costo;
    }

    public boolean equals(Libro libro1, Libro libro2){
        boolean equals=false;

        if(libro1!=null&&libro2!=null&&libro1.getAutore().equals(libro2.getAutore())&&libro1.getTitolo().equals(libro2.getTitolo())){
            equals=true;
        }

        return equals;
    }
    
    public String toString() {
        return "{" +
            " titolo='" + getTitolo() + "'" +
            ", autore='" + getAutore() + "'" +
            ", numeroPagine='" + getNumeroPagine() + "'" +
            "}";
    }
}
