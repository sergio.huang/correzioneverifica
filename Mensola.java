public class Mensola{
    private final int MAX_NUM_VOLUMI=15;
    private Libro[] volumi;

    public Mensola(){
        volumi=new Libro[MAX_NUM_VOLUMI];
    }
    public Mensola(Libro[] libri){
        volumi=new Libro[MAX_NUM_VOLUMI];
        for (int pos = 0; pos < MAX_NUM_VOLUMI; pos++) {
            if(libri[pos]!=null){
            volumi[pos]=libri[pos];
            }
        }
    }
    public boolean equals(Mensola mensola2){
        int i=0;
        boolean equals=true;
        while(equals==true&&i<mensola2.MAX_NUM_VOLUMI-1){
            if(getVolume(i)!=null&&!getVolume(i).getTitolo().equals(mensola2.getVolume(i).getTitolo())){
                equals=false;
            }
            i++;
        }
        return equals;
    }
    public int setVolume(Libro libro, int pos){
        if((pos<0)&&(pos>=MAX_NUM_VOLUMI))
            return -1;
        if (volumi[pos]!=null)
            return -2;
        volumi[pos]=libro;
        return pos;
    }
    public Libro getVolume(int pos){
        if ((pos<0)&&(pos>=MAX_NUM_VOLUMI))
            return null;
        return volumi[pos];        
    }
    public int rimuoviVolume(int pos){
        if((pos<0)&&(pos>=MAX_NUM_VOLUMI))
            return -1;
        if (volumi[pos]==null)
            return -2;
        volumi[pos]=null;
        return pos;
    }
    public int getNumMaxVolumi(){
        return MAX_NUM_VOLUMI;
    }
    public boolean compatta(){
        boolean compattato=false;
        int conta=0;
        for (int i = 0; i < volumi.length; i++) {
            if(volumi[i]!=null){
                volumi[conta]=volumi[i];
                conta++;
            }
            compattato=true;
        }
        return compattato;
    }
    public String[] elencoLibriAutore(String autore){
        int conta=0;
        for (int i = 0; i < volumi.length; i++){
            if (getVolume(i).getAutore().equals(autore)&&getVolume(i).getAutore()!=null){
                conta++;
            }
        }
        String[] elenco=new String[conta];
        conta=0;
        for (int i = 0; i < volumi.length; i++){
            if (getVolume(i).getAutore()!=null&&getVolume(i).getAutore().equals(autore)){
                elenco[conta]=getVolume(i).getTitolo();
                conta++;
            }
        }
        return elenco;
    }

    public int getNumVolumi(){
        int n=0;
        for (int i = 0; i < volumi.length; i++) {
            n++;
        }
        return n;
    }
    public String toString(){
        String s="";
        for (int i = 0; i < MAX_NUM_VOLUMI; i++) {
            s += volumi[i] + System.lineSeparator();
        }
        return s;
    }
}